<?php

interface SortedLinkedListInterface
{
    /**
     * Returns sorted link as an array.
     *
     * @return array
     */
    public function toArray(): array;

    /**
     * Add new item to the end of the list.
     *
     * @param int|string $value
     * @return SortedLinkedListItem
     */
    public function append(int|string $value): SortedLinkedListItem;

    /**
     * Add new item to the front of the list.
     *
     * @param int|string $value
     * @return SortedLinkedListItem
     */
    public function prepend(int|string $value): SortedLinkedListItem;

    /**
     * Add an item after another one.
     *
     * @param int|string $value
     * @param SortedLinkedListItem $after
     * @return SortedLinkedListItem
     */
    public function insertAfter(int|string $value, SortedLinkedListItem $after): SortedLinkedListItem;

    /**
     * Removes item from the front of the list.
     *
     * @return SortedLinkedListItem|null
     */
    public function shift(): ?SortedLinkedListItem;

    /**
     * Returns value of item on given index.
     *
     * @param int $index
     * @return int|string
     */
    public function get(int $index): int|string;

    /**
     * Removes the item from the sorted list.
     *
     * @param SortedLinkedListItem $remove
     * @return SortedLinkedListItem
     */
    public function remove(SortedLinkedListItem $remove): SortedLinkedListItem;
}