<?php

class SortedLinkedListItem
{
    /**
     * @param int|string $value
     * @param SortedLinkedListItem|null $next
     */
    public function __construct(
        public int|string $value,
        public SortedLinkedListItem|null $next = null,
    ) {}
}