<?php

class SortedLinkedList implements SortedLinkedListInterface, Countable
{
    /**
     * @param SortedLinkedListItem|null $item
     */
    public function __construct(
        public SortedLinkedListItem|null $item = null
    ) {}

    /**
     * @inheritDoc
     */
    public function append(int|string $value): SortedLinkedListItem
    {
        // There is no item yet so we use prepend
        if (!$item = $this->item) {
            return $this->prepend($value);
        }

        // Otherwise we need to find the last item
        while ($item->next ?? null) {
            $item = $item->next;
        }

        // No we can insert the new item
        return $this->insertAfter($value, $item);
    }

    /**
     * @inheritDoc
     */
    public function prepend(int|string $value): SortedLinkedListItem
    {
        $this->item = new SortedLinkedListItem($value, $this->item);

        return $this->item;
    }

    /**
     * @inheritDoc
     */
    public function insertAfter(int|string $value, SortedLinkedListItem $after): SortedLinkedListItem
    {
        $after->next = new SortedLinkedListItem($value, $after->next);

        return $after->next;
    }

    /**
     * Returns all items of sorted list as an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        $ret = [];
        $item = $this->item;
        $i = 0;

        while ($item) {
            $ret[] = [
                'index' => $i++,
                'value' => $item->value,
            ];

            $item = $item->next;
        }

        return $ret;
    }

    /**
     * Returns count of all items.
     *
     * @return int
     */
    public function count(): int
    {
        $cnt = 0;
        $item = $this->item;

        while ($item) {
            $cnt++;
            $item = $item->next;
        }

        return $cnt;
    }

    /**
     * @inheritDoc
     */
    public function shift(): ?SortedLinkedListItem
    {
        if (!$node = $this->item) {
            return null;
        }

        if (!$this->item = $node->next) {
            return null;
        }

        $node->next = null;

        return $node;
    }

    /**
     * @inheritDoc
     */
    public function get(int $index): int|string
    {
        $item = $this->item;

        for ($i = 0; $i < $index; $i++) {
            if (!$item) {
                break;
            }

            $item = $item->next;
        }

        if (!$item) {
            throw new \Exception("A value of index \"{$index}\" was not found.");
        }

        return $item->value;
    }

    /**
     * @inheritDoc
     */
    public function remove(SortedLinkedListItem $remove): SortedLinkedListItem
    {
        $item = $this->item;

        if ($remove === $item && $this->count() === 1) {
            $this->item = null;

            return $item;
        }

        while ($item && $item->next !== $remove) {
            $item = $item->next;
        }

        if ($item) {
            $item->next = $remove->next;
        }

        $remove->next = null;

        return $remove;
    }
}