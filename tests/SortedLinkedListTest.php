<?php

use PHPUnit\Framework\TestCase;

final class SortedLinkedListTest extends TestCase
{
    public function testPrepend(): void
    {
        $linkedList = new SortedLinkedList();
        $linkedList->prepend('First');
        $linkedList->prepend('Second');

        $testArr = $linkedList->toArray();

        $this->assertCount(2, $testArr);
        $this->assertEquals('Second', $testArr[0]['value']);
    }

    public function testAppend(): void
    {
        $linkedList = new SortedLinkedList();
        $linkedList->append('First');
        $linkedList->append('Second');

        $testArr = $linkedList->toArray();

        $this->assertCount(2, $testArr);
        $this->assertEquals('First', $testArr[0]['value']);
    }

    public function testGet(): void
    {
        $linkedList = new SortedLinkedList();
        $linkedList->append('First');
        $linkedList->append('Second');

        $this->assertEquals('First', $linkedList->get(0));
        $this->assertEquals('Second', $linkedList->get(1));
    }

    public function testGetFailed(): void
    {
        $linkedList = new SortedLinkedList();
        $linkedList->append('First');
        $linkedList->append('Second');

        $this->expectException(\Exception::class);

        $linkedList->get(4);
    }

    public function testShift(): void
    {
        $linkedList = new SortedLinkedList();
        $linkedList->append('First');
        $linkedList->append('Second');
        $linkedList->append('Third');

        $item = $linkedList->shift();

        $this->assertEquals('First', $item->value);

        $testArr = $linkedList->toArray();

        $this->assertCount(2, $testArr);
        $this->assertEquals('Second', $linkedList->get(0));
        $this->assertEquals('Third', $linkedList->get(1));
    }

    public function testRemove(): void
    {
        $linkedList = new SortedLinkedList();
        $item1 = $linkedList->append('First');
        $item2 = $linkedList->append('Second');
        $item3 = $linkedList->append('Third');

        $linkedList->remove($item2);
        $linkedList->remove($item3);
        $linkedList->remove($item1);

        $testArr = $linkedList->toArray();

        $this->assertCount(0, $testArr);
    }
}