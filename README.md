# Example of Sorted Linked List

Example of simple implementation of the sorted linked list.

But in fact I recommend of using [SplDoublyLinkedList](https://www.php.net/manual/en/class.spldoublylinkedlist.php) or just plain array.

Running tests:

```shell
composer install
./vendor/bin/phpunit tests
```
